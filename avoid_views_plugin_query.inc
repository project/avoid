<?php

class avoid_views_plugin_query extends views_plugin_query_default {
  function db_query($query, $args = array()) {
    if (!empty($this->view->preview) && !preg_match('/^SELECT COUNT\(\*\) FROM \(.*\) count_alias$/s', $query)) {
      $result = db_query("EXPLAIN $query", $args);
      $this->avoid_handle_query($result);
    }
    return db_query($query, $args);
  }

  function db_query_range($query, $from, $count, $args = array()) {
    if (!empty($this->view->preview)) {
      $result = db_query_range("EXPLAIN $query", $from, $count, $args);
      $this->avoid_handle_query($result);
    }
    return db_query_range($query, $from, $count, $args);
  }

  function avoid_handle_query($result) {
    $this->avoid_process_view();
    $this->avoid_process_explain($result);
  }

  function avoid_process_view() {
    $tables = array();
    $display_handler = $this->view->display_handler;
    $group_by = $display_handler->use_group_by();
    foreach (array('filters', 'sorts', 'arguments', 'fields') as $option_name) {
      foreach ($display_handler->get_option($option_name) as $option_data) {
        if ($option_name != 'fields' || $group_by) {
          $tables[$option_data['table']] = $option_data['table'];
        }
      }
    }
    foreach ($this->table_queue as $table) {
      if (!empty($table['join']->definition['extra'])) {
        $tables[$table['table']] = $table['table'];
      }
    }
    if (count($tables) > 1) {
      drupal_set_message(t('Avoid: more than one table (@tables) has condition / sort on it.', array('@tables' => implode(', ', $tables))), 'warning');
    }
  }

  function avoid_process_explain($result) {
    while ($explain_row = db_fetch_object($result)) {
      $pieces = explode('; ', $explain_row->Extra);
      foreach ($pieces as $piece) {
        switch ($piece) {
          case 'Using temporary':
            drupal_set_message(t('Avoid: temporary is used for table @table (alias @alias)', array('@table' => $this->table_queue[$explain_row->table]['table'], '@alias' => $explain_row->table)), 'warning');
            break;
          case 'Using filesort':
            drupal_set_message(t('Avoid: filesort is used for table @table (alias @alias)', array('@table' => $this->table_queue[$explain_row->table]['table'], '@alias' => $explain_row->table)), 'warning');
            break;
        }
      }
    }
  }
}
